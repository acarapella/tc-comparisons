<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.1.9
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2014 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_Block_Tracking_Customer extends Listrak_Remarketing_Block_Abstract
{
    private $_suspend = false;

    public function _toHtml() {
        try {
            if (!$this->_suspend) {
                $custSession = Mage::getSingleton('customer/session');
                if ($custSession->isLoggedIn() && !$custSession->getListrakCustomerTracked()) {
                    $cust = $custSession->getCustomer();

                    $this->addLine("_ltk.SCA.SetCustomer("
                        . $this->toJsString($cust->getEmail()) . ", "
                        . $this->toJsString($cust->getFirstname()) . ", "
                        . $this->toJsString($cust->getLastname()) . ");");
                    $this->addLine("_ltk.SCA.Submit();");

                    $custSession->setListrakCustomerTracked(true);
                }
            }

            return parent::_toHtml();
        } catch(Exception $e) {
            Mage::getModel('listrak/log')->addException($e);
            return '';
        }
    }
    
    public function onOrderConfirmationPage() {
        Mage::getSingleton('customer/session')->unsListrakCustomerTracked();
        $this->_suspend = true;
    }
}
