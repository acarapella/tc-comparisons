<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.1.9
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2014 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_Block_Tracking_Activity extends Listrak_Remarketing_Block_Abstract
{
    private $_browsingSku = null;

    public function _toHtml() {
        try {
            if ($this->_browsingSku)
                $this->addLine("_ltk.Activity.AddProductBrowse({$this->toJsString($this->_browsingSku)});");
            else
                $this->addLine("_ltk.Activity.AddPageBrowse({$this->toJsString(Mage::helper('core/url')->getCurrentUrl())});");
            $this->addLine("_ltk.Activity.Submit();");

            return parent::_toHtml();
        } catch(Exception $e) {
            Mage::getModel('listrak/log')->addException($e);
            return '';
        }
    }

    public function onProductPage() {
        $p = Mage::registry('current_product');
        if ($p)
            $this->_browsingSku = $p->getSku();
    }
}
