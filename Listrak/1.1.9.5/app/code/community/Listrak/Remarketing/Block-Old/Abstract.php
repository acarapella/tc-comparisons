<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.1.9
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2014 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_Block_Abstract extends Mage_Core_Block_Abstract
{
    private $_lines = array();

    public function _toHtml() {
        try {
            $js = "";
            foreach($this->_lines as $line)
                $js .= $line . "\n";
            return $js;
        }
        catch(Exception $e) {
            Mage::getModel('remarketing/log')->addException($e);
            return '';
        }
    }

    protected function addLine($js) {
        $this->_lines[] = $js;
    }

    public function jsEscape($value, $quote = "'") {
        return addcslashes($value, "\\{$quote}");
    }

    public function toJsString($value) {
        return "'{$this->jsEscape($value)}'";
    }
}
