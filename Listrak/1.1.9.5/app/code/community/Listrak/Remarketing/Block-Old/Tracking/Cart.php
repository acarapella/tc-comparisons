<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.1.9
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2014 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_Block_Tracking_Cart extends Listrak_Remarketing_Block_Abstract
{
    private $_cartPage = false;
    private $_confirmationPage = false;
    private $_convert = false;

    public function _toHtml() {
        try {
            if ($this->_confirmationPage)
                return "";

            $needSubmit = false;

            if ($this->_convert) {
                $session = Mage::getSingleton('listrak/session');
                $this->addLine("_ltk.SCA.SetSessionID({$this->toJsString($session->getSessionId())});");

                $emails = $session->getEmails();
                if (count($emails) > 0 && !Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $this->addLine("_ltk.SCA.SetCustomer({$this->toJsString($emails[0]['email'])}, '', '');");
                    $needSubmit = true;
                }
            }

            $chkSession = Mage::getSingleton('checkout/session');
            if ($this->_cartPage || $this->_convert || $chkSession->getListrakCartModified()) {
                $this->addLine("_ltk.SCA.Stage = 1;");

                if (Mage::getSingleton('checkout/cart')->getSummaryQty() > 0) {
                    foreach($this->getCartItems() as $item) {
                        $this->addLine("_ltk.SCA.AddItemWithLinks("
                             . $this->toJsString($item->getSku()) . ", "
                             . $this->toJsString($item->getQty()) . ", "
                             . $this->toJsString($item->getPrice()) . ", "
                             . $this->toJsString($item->getName()) . ", "
                             . $this->toJsString($item->getImageUrl()) . ", "
                             . $this->toJsString($item->getProductUrl()) . ");");
                    }

                    $ltksid = $this->getBasketId();
                    $this->addLine("_ltk.SCA.Meta1 = {$this->toJsString($ltksid)};");
                    $chkSession->setCartLtksid($ltksid);

                    $this->addLine("_ltk.SCA.Submit();");
                }
                else {
                    $this->addLine("_ltk.SCA.ClearCart();");
                    // _ltk.SCA.Submit is called by _ltk.SCA.ClearCart
                }

                $chkSession->unsListrakCartModified();
            }
            else if ($needSubmit) {
                $this->addLine("_ltk.SCA.Submit();");
            }

            return parent::_toHtml();
        } catch(Exception $e) {
            Mage::getModel('listrak/log')->addException($e);
            return '';
        }
    }

    public function getCartItems() {
        $result = array();

        $productHelper = Mage::helper('remarketing/product');
        foreach (Mage::getSingleton('checkout/cart')->getQuote()->getAllVisibleItems() as $item) {
            $info = $productHelper->getProductInformationFromQuoteItem($item, array('product_url', 'image_url'));

            $item->setSku($info->getSku());
            $item->setProductUrl($info->getProductUrl());
            $item->setImageUrl($info->getImageUrl());

            $result[] = $item;
        }

        return $result;
    }

    public function getBasketId() {
        $storeId = Mage::app()->getStore()->getStoreId();
        $quoteId = Mage::getSingleton('checkout/session')->getQuoteId();

        $str = $storeId . ' ' . $quoteId;
        while(strlen($str) < 16) // 5 for store ID, 1 for the space, and 10 for the quote ID
            $str .= ' ' . $quoteId;
        $str = substr($str, 0, 16);

        return Mage::helper('remarketing')->urlEncrypt($str);
    }
    
    public function convertDbSession() {
        $this->_convert = true;
    }

    public function onCartPage() {
        $this->_cartPage = true;
    }
    
    public function onOrderConfirmationPage() {
        $this->_confirmationPage = true;
    }
}
