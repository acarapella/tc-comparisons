<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.1.9
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2014 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_Block_Conversion_Cart extends Listrak_Remarketing_Block_Conversion_Abstract
{
    public function _toHtml() {
        try {
            $this->addLine("_ltk.SCA.Stage = 7;");
            $this->addLine("_ltk.SCA.OrderNumber = {$this->toJsString($this->getOrderConfirmationNumber())};");
            $this->addLine("_ltk.SCA.SetCustomer("
                . $this->toJsString($this->getEmailAddress()) . ", "
                . $this->toJsString($this->getFirstName()) . ", "
                . $this->toJsString($this->getLastName()) . ");");
            $this->addLine("_ltk.SCA.Submit();");

            return parent::_toHtml();
        } catch(Exception $e) {
            Mage::getModel('listrak/log')->addException($e);
            return '';
        }
    }
}
