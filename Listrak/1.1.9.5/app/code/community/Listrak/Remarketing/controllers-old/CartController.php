<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.0.0
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2011 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_CartController extends Mage_Core_Controller_Front_Action
{
    private $_ltkSession = false;

    public function indexAction()
    {
        return $this;
    }

    public function reloadAction()
    {
        $checkout = Mage::getSingleton('checkout/session');
        $cust = Mage::getSingleton('customer/session');
        $chkQuote = Mage::helper('checkout/cart')->getQuote();

        try {
            $ltksid = $this->_getLtksid();
            if (!$ltksid) {
                return $this->_redirectAfterReload();
            }
            
            if ($this->_isUid($ltksid)) {
                $ltksidcookie = Mage::getModel('core/cookie')->get('ltksid');
                if (!Mage::helper('remarketing')->trackingTablesExist() || (!empty($ltksidcookie) && $ltksidcookie == $ltksid && $chkQuote && $chkQuote->getId())) {
                    return $this->_redirectAfterReload();
                }

                $ltksession = $this->_getSession();
                if ($ltksession && $ltksession->getQuoteId() && $cust && $cust->isLoggedIn() && $cust->getId() === $ltksession->getCustomerId()) {
                    return $this->_redirectAfterReload();
                }
            }
            else {
                if (($checkout->getCartLtksid() == $ltksid || $checkout->getMergedLtksid() == $ltksid) && Mage::getSingleton('checkout/cart')->getSummaryQty() > 0) {
                    return $this->_redirectAfterReload();
                }
            }

            $quote = $this->_getQuote();
            if ($quote && $quote->getId() && $quote->getId() != $checkout->getQuoteId() && $quote->getIsActive()) {
                if (!$chkQuote) {
                    $chkQuote = Mage::getModel('sales/quote');
                }

                $chkQuote->merge($quote)
                    ->collectTotals()
                    ->save();
                $checkout->setQuoteId($chkQuote->getId());
                $checkout->setMergedLtksid($ltksid);
            }
        } catch (Exception $ex) {
            Mage::getModel("listrak/log")->addException($ex);
        }

        return $this->_redirectAfterReload();
    }
    
    private function _getLtksid()
    {
        return $this->getRequest()->getParam('ltksid');
    }
    
    private function _isUid($str) {
        return preg_match('/^[\da-f]{8}-[\da-f]{4}-[\da-f]{4}-[\da-f]{4}-[\da-f]{12}$/i', $str);
    }

    private function _getSession()
    {
        if ($this->_ltkSession === false) {
            $sid = $this->_getLtksid();

            if(Mage::helper('remarketing')->trackingTablesExist() && $this->_isUid($sid))
            {
                $ltksession = Mage::getModel("listrak/session")->setSessionId($sid);
                $ltksession->getResource()->loadBySessionId($ltksession);

                if ($ltksession->hasQuoteId())
                    $this->_ltkSession = $ltksession;
            }

            if ($this->_ltkSession === false)
                $this->_ltkSession = null;
        }

        return $this->_ltkSession;
    }

    private function _getQuote()
    {
        $session = $this->_getSession();
        if ($session) {
            $storeId = Mage::app()->getStore()->getStoreId();
            $quoteId = $session->getQuoteId();
        }
        else {
            $sid = $this->_getLtksid();
            $qid = Mage::helper('remarketing')->urlDecrypt($sid);

            $parts = explode(' ', $qid, 2);
            if (sizeof($parts) > 1) {
                $storeId = intval($parts[0]);
                $quoteId = intval($parts[1]);
            }
        }

        if (isset($storeId) && isset($quoteId)) {
            $quote = Mage::getModel('sales/quote')->setStoreId($storeId)->load($quoteId);
            if ($quote->getEntityId())
                return $quote;
        }

        return null;
    }

    private function _redirectAfterReload()
    {
        $qs = $this->getRequest()->getParams();
        unset($qs["redirectUrl"]);
        unset($qs["ltksid"]);

        $url = $this->getRequest()->getParam('redirectUrl');
        if (!$url) {
            $url = 'checkout/cart/';
        }

        return $this->_redirect(
            $url,
            array('_query' => $qs, '_secure' => Mage::app()->getStore()->isCurrentlySecure())
        );
    }
}
