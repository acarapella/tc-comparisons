<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.1.9
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2014 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

$installer = $this;
$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('listrak/session')}
  ADD COLUMN `converted` boolean NOT NULL DEFAULT 0;
");

try {
    Mage::getModel("listrak/log")->addMessage("1.1.8-1.1.9 upgrade");
} catch (Exception $e) {
}

try {
    $client = new Varien_Http_Client("http://magento.listrakbi.com/Install.ashx");
    $client->setMethod(Varien_Http_Client::POST);
    $client->setParameterPost("Listrak Extension Version", "1.1.9");
    $client->setParameterPost("Magento Version", Mage::getVersion());
    $client->setParameterPost("Install URL", "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
    $client->setParameterPost("IP Address", "$_SERVER[SERVER_ADDR]");
    $client->request();
} catch (Exception $e) {
}

$installer->endSetup();
