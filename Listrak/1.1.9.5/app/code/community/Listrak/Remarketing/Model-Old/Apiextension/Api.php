<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.0.0
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2011 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_Model_Apiextension_Api
    extends Mage_Api_Model_Resource_Abstract
{
    private $_attributesMap = array(
        'order' => array('order_id' => 'entity_id')
    );

    public function subscribers($storeId = 1, $startDate = null, $perPage = 50, $page = 1)
    {
        Mage::helper('remarketing')->requireCoreEnabled();

        try {
            if ($startDate === null || !strtotime($startDate)) {
                $this->_fault('incorrect_date');
            }

            $result = array();

            $collection = Mage::getModel("listrak/apiextension")
                ->getResource()
                ->subscribers($storeId, $startDate, $perPage, $page);

            foreach ($collection as $item) {
                $result[] = $item;
            }

            return $result;
        } catch (Exception $e) {
            throw Mage::helper('remarketing')->generateAndLogException("Exception occurred in API call: " . $e->getMessage(), $e);
        }
    }

    public function subscribersPurge($endDate = null)
    {
        try {
            if ($endDate === null || !strtotime($endDate)) {
                $this->_fault('incorrect_date');
            }

            $subscriberupdates = Mage::getModel("listrak/subscriberupdate")
                ->getCollection()
                ->addFieldToFilter('updated_at', array('lt' => $endDate));

            $count = 0;

            foreach ($subscriberupdates as $subscriberupdate) {
                $subscriberupdate->delete();
                $count++;
            }

            return $count;
        } catch (Exception $e) {
            throw Mage::helper('remarketing')->generateAndLogException("Exception occurred in API call: " . $e->getMessage(), $e);
        }
    }

    public function customers($storeId = 1, $websiteId = 1, $perPage = 50, $page = 1)
    {
        Mage::helper('remarketing')->requireCoreEnabled();

        try {
            Mage::app()->setCurrentStore($storeId);

            $collection = Mage::getModel('customer/customer')->getCollection()
                ->addFieldToFilter('store_id', $storeId)
                ->addAttributeToSelect('*')
                ->setPageSize($perPage)
                ->setCurPage($page);

            $results = array();

            foreach ($collection as $customer) {
                $results[] = $this->_getCustomerArray($storeId, $customer);
            }

            return $results;
        } catch (Exception $e) {
            throw Mage::helper('remarketing')->generateAndLogException("Exception occurred in API call: " . $e->getMessage(), $e);
        }
    }

    private function _getCustomerArray($storeId, Mage_Customer_Model_Customer $customer)
    {
        $fields = array('entity_id' => '', 'firstname' => '', 'lastname' => '',
            'email' => '', 'website_id' => '', 'store_id' => '', 'group_id' => '',
            'gender_name' => '', 'dob' => '', 'group_name' => '');
        Mage::helper('remarketing')->setGroupNameAndGenderNameForCustomer($customer);
        $result = array_intersect_key($customer->toArray(), $fields);
        
        $metas = $this->_getCustomerMetas($storeId, $customer);
        if ($metas) {
            //if (isset($metas['meta1'])) $result['meta1'] = $metas['meta1'];
            if (isset($metas['meta2'])) $result['meta2'] = $metas['meta2'];
            if (isset($metas['meta3'])) $result['meta3'] = $metas['meta3'];
            if (isset($metas['meta4'])) $result['meta4'] = $metas['meta4'];
            if (isset($metas['meta5'])) $result['meta5'] = $metas['meta5'];
        }
        
        return $result;
    }

    public function orderStatus($storeId = 1, $startDate = null, $endDate = null,
        $perPage = 50, $page = 1, $filters = null
    )
    {
        Mage::helper('remarketing')->requireCoreEnabled();

        try {
            Mage::app()->setCurrentStore($storeId);

            $productsUpdated = array();

            $collection = Mage::getModel("sales/order")->getCollection()
                ->addFieldToFilter('store_id', $storeId)
                ->addFieldToFilter('updated_at', array('from' => $startDate, 'to' => $endDate))
                ->addFieldToFilter('status', array('neq' => 'pending'))
                ->setPageSize($perPage)->setCurPage($page)
                ->setOrder('updated_at', 'ASC');

            if (!Mage::helper('remarketing')->getMetaDataProvider())
                $collection->addAttributeToSelect('entity_id')
                    ->addAttributeToSelect('increment_id')
                    ->addAttributeToSelect('status')
                    ->addAttributeToSelect('updated_at');

            if (is_array($filters)) {
                try {
                    foreach ($filters as $field => $value) {
                        if (isset($this->_attributesMap['order'][$field])) {
                            $field = $this->_attributesMap['order'][$field];
                        }

                        $collection->addFieldToFilter($field, $value);
                    }
                } catch (Mage_Core_Exception $e) {
                    $this->_fault('filters_invalid', $e->getMessage());
                }
            }

            $results = array();

            foreach ($collection as $order) {
                $result = array();
                $result['increment_id'] = $order->getIncrementId();
                $result['status'] = $order->getStatus();
                $result['updated_at'] = $order->getUpdatedAt();

                $metas = $this->_getOrderMetas($storeId, $order);
                if ($metas) {
                    if (isset($metas['meta1'])) $result['meta1'] = $metas['meta1'];
                    if (isset($metas['meta2'])) $result['meta2'] = $metas['meta2'];
                    if (isset($metas['meta3'])) $result['meta3'] = $metas['meta3'];
                    if (isset($metas['meta4'])) $result['meta4'] = $metas['meta4'];
                    if (isset($metas['meta5'])) $result['meta5'] = $metas['meta5'];
                }

                $shipment = $order->getShipmentsCollection()->getFirstItem();
                if ($shipment) {
                    $tracks = $shipment->getAllTracks();
                    if (count($tracks) > 0) {
                        $result['tracking_number'] = $tracks[0]->getNumber();
                        $result['carrier_code'] = $tracks[0]->getCarrierCode();
                    }
                }

                $quantities = array();
                foreach($order->getAllVisibleItems() as $item) {
                    $info = Mage::helper('remarketing/product')
                        ->getProductInformationFromOrderItem($item, array('product'));

                    if (!in_array($info->getProductId(), $productsUpdated, true)) {
                        $product = Mage::getModel('catalog/product')->load($info->getProductId());
                        if ($product) {
                            $quantity = array();

                            $quantity['sku'] = $product->getSku();
                            $quantity['in_stock'] = $product->isAvailable() ? "true" : "false";
                            $stockItem = $product->getStockItem();
                            if ($stockItem) {
                                $quantity['qty_on_hand'] = $stockItem->getStockQty();
                            }

                            $quantities[] = $quantity;
                        }

                        $productsUpdated[] = $info->getProductId();
                    }
                }
                $result['quantities'] = $quantities;

                $results[] = $result;
            }

            return $results;
        } catch (Exception $e) {
            throw Mage::helper('remarketing')->generateAndLogException("Exception occurred in API call: " . $e->getMessage(), $e);
        }
    }

    public function orders($storeId = 1, $startDate = null, $endDate = null,
        $perPage = 50, $page = 1
    )
    {
        Mage::helper('remarketing')->requireCoreEnabled();

        try {
            Mage::app()->setCurrentStore($storeId);

            if ($startDate === null || !strtotime($startDate)) {
                $this->_fault('incorrect_date');
            }

            if ($endDate === null || !strtotime($endDate)) {
                $this->_fault('incorrect_date');
            }

            $orders = Mage::getModel('sales/order')->getCollection()
                ->addFieldToFilter('created_at', array('from' => $startDate, 'to' => $endDate))
                ->addFieldToFilter('store_id', $storeId)
                ->setPageSize($perPage)->setCurPage($page)
                ->setOrder('created_at', 'ASC');

            $results = array();

            foreach ($orders as $order) {
                $result = array();
                $result['info']['entity_id'] = $order->getEntityId();
                $result['info']['order_id'] = $order->getIncrementId();
                $result['info']['status'] = $order->getStatus();;
                $result['info']['customer_firstname'] = $order->getCustomerFirstname();
                $result['info']['customer_lastname'] = $order->getCustomerLastname();
                $result['info']['customer_email'] = $order->getCustomerEmail();
                $result['info']['subtotal'] = $order->getSubtotal();
                $result['info']['discount_amount'] = $order->getDiscountAmount();
                $result['info']['tax_amount'] = $order->getTaxAmount();
                $result['info']['shipping_amount'] = $order->getShippingAmount();
                $result['info']['grand_total'] = $order->getGrandTotal();
                $result['info']['coupon_code'] = $order->getCouponCode();
                $result['info']['billing_firstname'] = $order->getBillingFirstname();
                $result['info']['created_at'] = $order->getCreatedAt();
                $result['info']['updated_at'] = $order->getUpdatedAt();

                $metas = $this->_getOrderMetas($storeId, $order);
                if ($metas) {
                    if (isset($metas['meta1'])) $result['info']['meta1'] = $metas['meta1'];
                    if (isset($metas['meta2'])) $result['info']['meta2'] = $metas['meta2'];
                    if (isset($metas['meta3'])) $result['info']['meta3'] = $metas['meta3'];
                    if (isset($metas['meta4'])) $result['info']['meta4'] = $metas['meta4'];
                    if (isset($metas['meta5'])) $result['info']['meta5'] = $metas['meta5'];
                }

                $shipping = $order->getShippingAddress();
                if ($shipping) {
                    $result['shipping_address']['firstname'] = $shipping->getFirstname();
                    $result['shipping_address']['lastname'] = $shipping->getLastname();
                    $result['shipping_address']['company'] = $shipping->getCompany();
                    $result['shipping_address']['street'] = implode(', ', $shipping->getStreet());
                    $result['shipping_address']['city'] = $shipping->getCity();
                    $result['shipping_address']['region'] = $shipping->getRegion();
                    $result['shipping_address']['postcode'] = $shipping->getPostcode();
                    $result['shipping_address']['country'] = $shipping->getCountry();
                }

                $billing = $order->getBillingAddress();
                if ($billing) {
                    $result['billing_address']['firstname'] = $billing->getFirstname();
                    $result['billing_address']['lastname'] = $billing->getLastname();
                    $result['billing_address']['company'] = $billing->getCompany();
                    $result['billing_address']['street'] = implode(', ', $billing->getStreet());
                    $result['billing_address']['city'] = $billing->getCity();
                    $result['billing_address']['region'] = $billing->getRegion();
                    $result['billing_address']['postcode'] = $billing->getPostcode();
                    $result['billing_address']['country'] = $billing->getCountry();
                }

                if (Mage::helper('remarketing')->trackingTablesExist()) {
                    $result['session'] = Mage::getModel("listrak/session")->load($order->getQuoteId(), 'quote_id');
                }

                $result['product'] = array();
                foreach ($order->getAllVisibleItems() as $item) {
                    $result['product'][] = $this->_getOrderItemProductEntity($storeId, $order, $item);
                }

                if ($order->getCustomerId()) {
                    $customer = Mage::getModel("customer/customer")->load($order->getCustomerId());
                    if ($customer) {
                        $result['customer'] = $this->_getCustomerArray($storeId, $customer);
                    }
                }

                $results[] = $result;
            }

            return $results;
        } catch (Exception $e) {
            throw Mage::helper('remarketing')->generateAndLogException("Exception occurred in API call: " . $e->getMessage(), $e);
        }
    }

    private function _getOrderItemProductEntity($storeId, Mage_Sales_Model_Order $order, Mage_Sales_Model_Order_Item $item)
    {
        $info = Mage::helper('remarketing/product')->getProductInformationFromOrderItem($item, array('product'));
        $productModel = $info->getProduct();

        $product = array();
        if ($productModel && $productModel->getId()) {
            $product['sku'] = $productModel->getSku();
            $product['name'] = $productModel->getName();
            $product['product_price'] = $productModel->getPrice();

            // Inventory
            $product['in_stock'] = $productModel->isAvailable() ? "true" : "false";
            $stockItem = $productModel->getStockItem();
            if ($stockItem) {
                $product['qty_on_hand'] = $stockItem->getStockQty();
            }
        } else {
            $product['sku'] = $item->getProductOptionByCode('simple_sku')
                ? $item->getProductOptionByCode('simple_sku') : $item->getSku();
            $product['name'] = $item->getName();
        }

        $product['price'] = $item->getPrice();
        $product['qty_ordered'] = $item->getQtyOrdered();

        $metas = $this->_getOrderItemMetas($storeId, $order, $item, $info->getProduct());
        if ($metas) {
            if (isset($metas['meta1'])) $product['meta1'] = $metas['meta1'];
            if (isset($metas['meta2'])) $product['meta2'] = $metas['meta2'];
            if (isset($metas['meta3'])) $product['meta3'] = $metas['meta3'];
            if (isset($metas['meta4'])) $product['meta4'] = $metas['meta4'];
            if (isset($metas['meta5'])) $product['meta5'] = $metas['meta5'];
        }

        if ($info->getIsBundle()) {
            $product['bundle_items'] = array();
            foreach ($item->getChildrenItems() as $childItem) {
                $product['bundle_items'][] = $this->_getOrderItemProductEntity($storeId, $order, $childItem);
            }
        }

        return $product;
    }

    private function _getCustomerMetas($storeId, Mage_Customer_Model_Customer $customer) {
        try {
            $provider = Mage::helper('remarketing')->getMetaDataProvider();
            if ($provider)
                return $provider->customer($storeId, $customer);
        }
        catch(Exception $e) {
            Mage::helper('remarketing')->generateAndLogException('Error retrieving customer meta data.', $e);
        }

        return null;
    }

    private function _getOrderMetas($storeId, Mage_Sales_Model_Order $order) {
        try {
            $provider = Mage::helper('remarketing')->getMetaDataProvider();
            if ($provider)
                return $provider->order($storeId, $order);
        }
        catch(Exception $e) {
            Mage::helper('remarketing')->generateAndLogException('Error retrieving order meta data.', $e);
        }

        return null;
    }

    private function _getOrderItemMetas($storeId, Mage_Sales_Model_Order $order, Mage_Sales_Model_Order_Item $orderItem, Mage_Catalog_Model_Product $product = null) {
        try {
            $provider = Mage::helper('remarketing')->getMetaDataProvider();
            if ($provider)
                return $provider->orderItem($storeId, $order, $orderItem, $product);
        }
        catch(Exception $e) {
            Mage::helper('remarketing')->generateAndLogException('Error retrieving order item meta data.', $e);
        }

        return null;
    }

    public function info($storeId)
    {
        try {
            Mage::app()->setCurrentStore($storeId);

            $result = array();
            $result["magentoVersion"] = Mage::getVersion();
            
            $module = Mage::getConfig()->getNode('modules')->Listrak_Remarketing;
            $core_resource = Mage::getModel('core/resource_resource');

            $result['listrakExtension']['active'] = (string)$module->active;
            $result['listrakExtension']['output'] = Mage::getStoreConfig("advanced/modules_disable_output/Listrak_Remarketing") == '0' ? 'true' : 'false';
            $result['listrakExtension']['version'] = (string)$module->version;
            if ($core_resource) {
                $result['listrakExtension']['install_version'] = $core_resource->getDbVersion('listrak_remarketing_setup');
                $result['listrakExtension']['data_version'] = $core_resource->getDataVersion('listrak_remarketing_setup');
            }

            $helper = Mage::helper('remarketing');
            $result["listrakSettings"] = array(
                "coreEnabled" => $helper->coreEnabled() ? "true" : "false",
                "onescriptEnabled" => $helper->onescriptEnabled() ? "true" : "false",
                "onescriptReady" => $helper->onescriptReady() ? "true" : "false",
                "trackingID" => Mage::getStoreConfig('remarketing/modal/listrakMerchantID'),
                "scaEnabled" => $helper->scaEnabled() ? "true" : "false",
                "activityEnabled" => $helper->activityEnabled() ? "true" : "false",
                "reviewsApiEnabled" => $helper->reviewsEnabled() ? "true" : "false",
                "trackingTablesExist" => $helper->trackingTablesExist() ? "true" : "false"
            );
            $result["ini"] = array();

            $subModel = Mage::getModel("newsletter/subscriber");
            $orderModel = Mage::getModel("sales/order");
            $productModel = Mage::getModel('catalog/product');

            $result["classes"] = get_class($subModel) . ',' . get_class($orderModel) .
                ',' . get_class($orderModel->getCollection()) . ',' .
                get_class($productModel) . ',' . get_class($productModel->getCollection());

            $ra = Mage::getSingleton('core/resource')->getConnection('core_read');
            $countQueryText = "select count(*) as c from " .
                Mage::getModel("listrak/subscriberupdate")->getResource()->getTable("listrak/subscriber_update");
            $numSubUpdates = $ra->fetchRow($countQueryText);
            
            if ($helper->trackingTablesExist()) {
                $countQueryText = "select count(*) as c from " .
                    Mage::getModel("listrak/session")->getResource()->getTable("listrak/session");
                $numSessions = $ra->fetchRow($countQueryText);
                $countQueryText = "select count(*) as c from " .
                    Mage::getModel("listrak/click")->getResource()->getTable("listrak/click");
                $numClicks = $ra->fetchRow($countQueryText);

                $result["counts"] = $numSessions['c'] . ',' . $numSubUpdates['c'] . ',' . $numClicks['c'];
            }
            else {
                $result["counts"] = $numSubUpdates['c'];
            }

            $result["modules"] = array();
            $modules = (array)Mage::getConfig()->getNode('modules')->children();

            foreach ($modules as $key => $value) {
                $valueArray = $value->asCanonicalArray();
                $version = (isset($valueArray["version"])) ? $valueArray["version"] : '';
                $active = (isset($valueArray["active"])) ? $valueArray["active"] : '';
                $result["modules"][] = "name=$key, version=" . $version .", isActive=" . $active;
            }

            $ini = array("session.gc_maxlifetime", "session.cookie_lifetime",
                "session.gc_divisor", "session.gc_probability");

            foreach ($ini as $iniParam) {
                $result["ini"][] = "$iniParam=" . ini_get($iniParam);
            }

            return $result;
        } catch (Exception $e) {
            throw Mage::helper('remarketing')->generateAndLogException("Exception occurred in API call: " . $e->getMessage(), $e);
        }
    }
}
