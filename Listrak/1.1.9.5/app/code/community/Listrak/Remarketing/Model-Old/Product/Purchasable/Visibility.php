<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.1.9
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2014 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_Model_Product_Purchasable_Visibility
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'off', 'label' => 'Do Not Filter'),
            //array('value' => 'no', 'label' => 'Not Visible Individually'),
            array('value' => 'catalog', 'label' => 'Catalog'),
            array('value' => 'search', 'label' => 'Search'),
            array('value' => 'both', 'label' => 'Catalog and Search'),
            array('value' => 'site', 'label' => 'Site (Catalog or Search)')
        );
    }

    public function isProductPurchasableBySetting($setting, $product) {
        switch($setting) {
            case 'off': return true;
            //case 'no': return $product->getVisibility() == Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE;
            case 'catalog': return $product->isVisibleInCatalog();
            case 'search': return in_array($product->getVisibility(), Mage::getSingleton('catalog/product_visibility')->getVisibleInSearchIds());
            case 'both': return $product->getVisibility() == Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
            case 'site': return $product->isVisibleInSiteVisibility();
            default: return true;
        }
    }

    public function getVisibilityFilterFromSetting($setting) {
        switch($setting) {
            case 'off': return null;
            //case 'no': return Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE;
            case 'catalog': return array('in' => Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
            case 'search': return array('in' => Mage::getSingleton('catalog/product_visibility')->getVisibleInSearchIds());
            case 'both': return Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
            case 'site': return array('in' => Mage::getSingleton('catalog/product_visibility')->getVisibleInSiteIds());
            default: return null;
        }
    }
}

