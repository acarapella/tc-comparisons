<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.1.9
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2014 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_Model_Product_Api
    extends Mage_Api_Model_Resource_Abstract
{
    public function products($storeId = 1, $perPage = 50, $page = 1)
    {
        Mage::helper('remarketing')->requireCoreEnabled();

        if (!is_numeric($storeId) || !is_numeric($perPage) || !is_numeric($page))
            throw new Exception("Bad request parameters.");

        try {
            Mage::app()->setCurrentStore($storeId);

            $collection = Mage::getModel('catalog/product')->getCollection()
                ->addStoreFilter($storeId)
                ->addAttributeToSelect('*')
                ->setPageSize($perPage)
                ->setCurPage($page)
                ->load();

            Mage::getModel('cataloginventory/stock')->addItemsToProducts($collection);

            $results = array();

            foreach ($collection as $product) {
                $results[] = Mage::helper('remarketing/product')->getProductEntity($product, $storeId);
            }

            return $results;
        } catch (Exception $e) {
            throw Mage::helper('remarketing')->generateAndLogException("Exception occurred in API call: " . $e->getMessage(), $e);
        }
    }

    public function updates($storeId = 1, $startDate = null, $endDate = null, $perPage = 50, $page = 1)
    {
        Mage::helper('remarketing')->requireCoreEnabled();
        
        $dte1 = strtotime($startDate);
        $dte2 = strtotime($endDate);
        if (!is_numeric($storeId) || !$dte1 || !$dte2 || !is_numeric($perPage) || !is_numeric($page))
            throw new Exception("Bad request parameters.");

        try {
            Mage::app()->setCurrentStore($storeId);

            $collection = Mage::getModel('catalog/product')->getCollection()
                ->addStoreFilter($storeId)
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('updated_at', array('from' => $dte1, 'to' => $dte2, 'date' => true))
                ->setPageSize($perPage)
                ->setCurPage($page);
            $collection->getSelect()
                ->order(array('e.updated_at ASC', 'e.entity_id ASC'));
            $collection->load();

            Mage::getModel('cataloginventory/stock')->addItemsToProducts($collection);

            $results = array();

            foreach ($collection as $product) {
                $result = Mage::helper('remarketing/product')->getProductEntity($product, $storeId);
                $result['updated_at'] = $product->getUpdatedAt();
                $results[] = $result;
            }

            return $results;
        } catch (Exception $e) {
            throw Mage::helper('remarketing')->generateAndLogException("Exception occurred in API call: " . $e->getMessage(), $e);
        }
    }

    public function purchasable($storeId = 1, $perPage = 50, $page = 1)
    {
        Mage::app()->setCurrentStore($storeId);

        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addStoreFilter($storeId)
            ->addAttributeToSelect('visibility', 'left')
            ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);

        $purchasableFilter = Mage::getSingleton('listrak/product_purchasable_visibility')->getVisibilityFilterFromSetting(Mage::getStoreConfig('remarketing/productcategories/purchasable_visibility'));
        if ($purchasableFilter) {
            $dbRead = Mage::getSingleton('core/resource')->getConnection('core_read');

            $productToParentQuery = $dbRead->select()
                ->from(Mage::getResourceSingleton('catalog/product_type_configurable')->getMainTable(), array('product_id', 'parent_id'))
                ->group('product_id');

            $parentCollection = Mage::getModel('catalog/product')->getCollection()
                ->addStoreFilter($storeId)
                ->addAttributeToSelect('visibility', 'left')
                ->addAttributeToSelect('status', 'inner');

            try {
                return $this->_retrievePurchasablePage(
                    $this->_purchasableQueryHelper($dbRead, $collection, $productToParentQuery, $parentCollection, $purchasableFilter, 1),
                    $page, $perPage
                );
            }
            catch(Exception $e) {
                return $this->_retrievePurchasablePage(
                    $this->_purchasableQueryHelper($dbRead, $collection, $productToParentQuery, $parentCollection, $purchasableFilter, 2),
                    $page, $perPage
                );
            }
        }
        else {
            $query = $collection->getSelect();
            $query->reset(Zend_Db_Select::COLUMNS)
                ->columns(array('e.entity_id', 'e.sku'));
            return $this->_retrievePurchasablePage($query, $page, $perPage);
        }
    }

    private function _purchasableQueryHelper($dbRead, $productCollection, $productToParentQuery, $parentCollection, $purchasableFilter, $version) {
        $productQuery = $productCollection->getSelect();
        if ($version == 2) {
            $productQuery->reset(Zend_Db_Select::COLUMNS)->columns(array('e.entity_id', 'e.sku', 'e.attribute_set_id', 'e.type_id', 'e.visibility'));
        }

            $query = $dbRead->select();
        $query->from(array('product' => new Zend_Db_Expr("({$productQuery})")), array('entity_id', 'sku'))
                ->joinLeft(array('product_to_parent' => new Zend_Db_Expr("({$productToParentQuery})")), 'product_to_parent.product_id = product.entity_id', array())
                ->joinLeft(array('parent' => new Zend_Db_Expr("({$parentCollection->getSelect()})")), $dbRead->quoteInto('parent.entity_id = product_to_parent.parent_id AND parent.type_id = ?', Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE), array())
                ->where('COALESCE(parent.visibility, product.visibility) IN (?)', $purchasableFilter)
                ->where('parent.status IS NULL OR parent.status = ?', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);

        return $query;
        }

    private function _retrievePurchasablePage($query, $page, $perPage) {
        $query->limit($perPage, $perPage * ($page - 1));
        return $query->query()->fetchAll();
    }
}
