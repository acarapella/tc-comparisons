<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.0.0
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2011 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_Model_Observer
{
    public function trackingInit($observer)
    {
        try {
            if (Mage::helper('remarketing')->legacyTracking()) {
                $session = Mage::getSingleton('listrak/session');
                $session->init(true);

                $click = Mage::getModel('listrak/click');
                $click->checkForClick();
            }
            else if (Mage::helper('remarketing')->onescriptTracking()) {
                $session = Mage::getSingleton('listrak/session')->loadFromCookie();
                if ($session->getId() && !$session->getConverted()) {
                    Mage::getSingleton('core/layout')->getUpdate()->addHandle('remarketing_tracking_onescript_convert');

                    $session->setConverted(true);
                    $session->save();

                    $session->deleteCookie();
                }
            }
        } catch (Exception $ex) {
            Mage::getModel("listrak/log")->addException($ex);
        }

        return $this;
    }

    public function orderPlaced($observer)
    {
        try {
            if (Mage::helper('remarketing')->legacyTracking()) {
                $cs = Mage::getSingleton('core/session');
                $cs->setIsListrakOrderMade(true);
                $session = Mage::getSingleton('listrak/session');
                $session->init();
            }
        } catch (Exception $ex) {
            Mage::getModel("listrak/log")->addException($ex);
        }

        return $this;
    }

    public function subscriberSaved($observer)
    {
        try {
            if (Mage::helper('remarketing')->coreEnabled()) {
                $s = $observer->getSubscriber();
                $su = Mage::getModel("listrak/subscriberupdate")->load($s->getSubscriberId(), 'subscriber_id');

                if (!$su->getData()) {
                    $su->setSubscriberId($s->getSubscriberId());
                }

                $su->setUpdatedAt(gmdate('Y-m-d H:i:s'));
                $su->save();
            }
        } catch (Exception $ex) {
            Mage::getModel("listrak/log")->addException($ex);
        }

        return $this;
    }

    public function reviewUpdated($observer)
    {
        try {
            if (Mage::helper('remarketing')->reviewsEnabled()) {
                $review = $observer->getObject();

                Mage::getModel('listrak/review_update')
                    ->markUpdated($review->getReviewId(), $review->getEntityId(), $review->getEntityPkValue());
            }
        } catch (Exception $e) {
            Mage::getModel("listrak/log")->addException($e);
        }

        return $this;
    }

    public function reviewDeleted($observer)
    {
        try {
            if (Mage::helper('remarketing')->reviewsEnabled()) {
                $review = $observer->getObject();

                Mage::getModel('listrak/review_update')
                    ->markDeleted($review->getReviewId(), $review->getEntityId(), $review->getEntityPkValue());
            }
        } catch (Exception $e) {
            Mage::getModel('listrak/log')->addException($e);
        }

        return $this;
    }

    public function cartModified($observer) {
        try {
            if (Mage::helper('remarketing')->scaEnabled()) {
                Mage::getSingleton('checkout/session')->setListrakCartModified(true);
            }
        } catch(Exception $e) {
            Mage::getModel('listrak/log')->addException($e);
        }

        return $this;
    }

    public function resetCustomerTracking() {
        Mage::getSingleton('customer/session')->unsListrakCustomerTracked();
    }
}
