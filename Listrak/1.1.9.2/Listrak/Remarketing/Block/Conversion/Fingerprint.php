<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.1.9
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2014 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_Block_Conversion_Fingerprint extends Listrak_Remarketing_Block_Require_Sca
{
    public function _toHtml() {
        try {
            if (!$this->canRender())
                return '';

            return '<img src="' . Mage::helper('remarketing')->getFingerprintImageUrl() . '" width="1" height="1" style="position: absolute" />';
        } catch(Exception $e) {
            Mage::getModel('listrak/log')->addException($e);
            return '';
        }
    }
}
