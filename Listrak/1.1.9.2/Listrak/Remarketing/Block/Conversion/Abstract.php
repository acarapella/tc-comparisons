<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.1.9
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2014 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_Block_Conversion_Abstract extends Listrak_Remarketing_Block_Require_Onescript
{
    private $_order, $_customer, $_billingAddress;

    public function _toHtml() {
        return parent::_toHtml();
    }

    private $_canRender = null;
    public function canRender() {
        if ($this->_canRender == null)
            $this->_canRender = parent::canRender() && $this->isOrderConfirmationPage();

        return $this->_canRender;
    }

    public function getOrderId() {
        return Mage::getSingleton('checkout/session')->getLastOrderId();
    }

    public function getOrder() {
        if(!$this->_order && $this->_order !== false) {
            $order = Mage::getModel('sales/order')->load($this->getOrderId());
            $this->_order = $order ? $order : false;
        }

        return $this->_order;
    }
    
    public function getOrderConfirmationNumber() {
        return $this->getOrder()->getIncrementId();
    }

    public function getOrderItems() {
        // fix the skus before returning the data
        $result = array();

        $productHelper = Mage::helper('remarketing/product');
        foreach($this->getOrder()->getAllVisibleItems() as $item) {
            $info = $productHelper->getProductInformationFromOrderItem($item);
            $item->setSku($info->getSku());

            $result[] = $item;
        }

        return $result;
    }

    public function getBillingAddress() {
        if (!$this->_billingAddress && $this->_billingAddress !== false) {
            $addr = $this->getOrder()->getBillingAddress();
            $this->_billingAddress = $addr ? $addr : false;
        }

        return $this->_billingAddress;
    }

    public function getCustomer() {
        if (!$this->_customer && $this->_customer !== false) {
            $customer = Mage::getModel('customer/customer')->load($this->getOrder()->getCustomerId());
            $this->_customer = $customer ? $customer : false;
        }

        return $this->_customer;
    }

    public function getEmailAddress() {
        if ($this->getCustomer()->getId()) {
            return $this->getCustomer()->getEmail();
        }
        else {
            return $this->getOrder()->getCustomerEmail();
        }
    }

    public function getFirstName() {
        if ($this->getCustomer()->getId()) {
            return $this->getCustomer()->getFirstname();
        }
        else {
            return $this->getBillingAddress()->getFirstname();
        }
    }

    public function getLastName() {
        if ($this->getCustomer()->getId()) {
            return $this->getCustomer()->getLastname();
        }
        else {
            return $this->getBillingAddress()->getLastname();
        }
    }
}
