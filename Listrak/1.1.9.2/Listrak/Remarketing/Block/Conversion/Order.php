<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.1.9
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2014 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_Block_Conversion_Order extends Listrak_Remarketing_Block_Conversion_Abstract
{
    public function _toHtml() {
        try {
            if (!$this->canRender())
                return '';

            $this->addLine("_ltk.Order.SetCustomer("
                . $this->toJsString($this->getEmailAddress()) . ", "
                . $this->toJsString($this->getFirstName()) . ", "
                . $this->toJsString($this->getLastName()) . ");");
            $this->addLine("_ltk.Order.OrderNumber = {$this->toJsString($this->getOrderConfirmationNumber())};");

            $order = $this->getOrder();
            $this->addLine("_ltk.Order.ItemTotal = {$this->toJsString($order->getSubtotal())};");
            //$this->addLine("_ltk.Order.DiscountTotal = {$this->toJsString($order->getDiscountAmount())};");
            $this->addLine("_ltk.Order.HandlingTotal = {$this->toJsString($order->getShippingAmount())};");
            $this->addLine("_ltk.Order.TaxTotal = {$this->toJsString($order->getTaxAmount())};");
            $this->addLine("_ltk.Order.OrderTotal = {$this->toJsString($order->getGrandTotal())};");

            foreach($this->getOrderItems() as $item)
                $this->addLine("_ltk.Order.AddItem("
                    . $this->toJsString($item->getSku()) . ", "
                    . $this->toJsString((int)$item->getQtyOrdered()) . ", "
                    . $this->toJsString($item->getPrice()) . ");");

            $this->addLine("_ltk.Order.Submit();");

            return parent::_toHtml();
        } catch(Exception $e) {
            Mage::getModel('listrak/log')->addException($e);
            return '';
        }
    }
}
