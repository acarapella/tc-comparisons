<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.1.9
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2014 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_Block_Base extends Mage_Core_Block_Template
{
    private $_alwaysRenderTemplate = false;
    private $_lines = array();

    public function _toHtml() {
        if ($this->getTemplate()) {
            if (!$this->_alwaysRenderTemplate && !trim($this->getScript()))
                return "";

            return parent::_toHtml();
        }
        else
            return $this->getScript();
    }

    public function canRender() {
        return true;
    }

    public function getScript($addWhitespace = true) {
        $js = "";
        foreach($this->_lines as $line) {
            $js .= $line . "\n";
            if ($addWhitespace) $js .= "        ";
        }

        return $js . $this->getChildHtml();
    }

    protected function addLine($js) {
        $this->_lines[] = $js;
    }

    public function jsEscape($value, $quote = "'") {
        return addcslashes($value, "\\{$quote}");
    }

    public function toJsString($value) {
        return "'{$this->jsEscape($value)}'";
    }
    
    public function setAlwaysRenderTemplate($val) {
        $this->_alwaysRenderTemplate = (bool)$val;
    }

    public function isProductPage() {
        return Mage::app()->getRequest()->getModuleName() == 'catalog' &&
               Mage::app()->getRequest()->getControllerName() == 'product' &&
               Mage::app()->getRequest()->getActionName() == 'view';
    }

    public function isCartPage() {
        return Mage::app()->getRequest()->getModuleName() == 'checkout' &&
               Mage::app()->getRequest()->getControllerName() == 'cart' &&
               Mage::app()->getRequest()->getActionName() == 'index';
    }
    
    public function isOrderConfirmationPage() {
        return Mage::app()->getRequest()->getModuleName() == 'checkout' &&
               (Mage::app()->getRequest()->getControllerName() == 'multishipping' || Mage::app()->getRequest()->getControllerName() == 'onepage') &&
               Mage::app()->getRequest()->getActionName() == 'success';
    }
}
