<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.1.9
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2014 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_Block_Require_Sca extends Listrak_Remarketing_Block_Require_Onescript
{
    public function _toHtml() {
        try {
            if (!$this->canRender())
                return '';

            return parent::_toHtml();
        } catch(Exception $e) {
            Mage::getModel('listrak/log')->addException($e);
            return '';
        }
    }

    private $_canRender = null;
    public function canRender() {
        if ($this->_canRender == null)
            $this->_canRender = parent::canRender() && Mage::helper('remarketing')->scaEnabled();

        return $this->_canRender;
    }
}
