<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.0.0
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2011 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_Helper_Product
    extends Mage_Core_Helper_Abstract
{
    private $_parentsById = array();
    private $_urlsById = array();
    private $_attributeSets = null;
    private $_categories = array();
    private $_useConfigurableParentImages = null;
    private $_skipCategories = null;

    public function getProductEntity(Mage_Catalog_Model_Product $product, $storeId, $includeExtras = true)
    {
        $result = array();

        $result['entity_id'] = $product->getEntityId();
        $result['sku'] = $product->getSku();
        $result['name'] = $product->getName();
        $result['price'] = $product->getPrice();
        $result['special_price'] = $product->getSpecialPrice();
        $result['special_from_date'] = $product->getSpecialFromDate();
        $result['special_to_date'] = $product->getSpecialToDate();
        $result['cost'] = $product->getCost();
        $result['description'] = $product->getDescription();
        $result['short_description'] = $product->getShortDescription();
        $result['weight'] = $product->getWeight();
        if ($product->isVisibleInSiteVisibility()) {
            $result['url_path'] = $this->_getProductUrlWithCache($product);
        }

        $parentProduct = $this->_getParentProduct($product);
        if ($parentProduct != null) {
            $result['parent_id'] = $parentProduct->getEntityId();
            $result['parent_sku'] = $parentProduct->getSku();

            if (!$product->isVisibleInSiteVisibility()) {
                $result['name'] = $parentProduct->getName();

                if ($parentProduct->isVisibleInSiteVisibility()) {
                    $result['url_path'] = $this->_getProductUrlWithCache($parentProduct);
                }
            }

            if ($includeExtras && Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE == $parentProduct->getTypeId()) {
                $result['purchasable'] = $this->_isPurchasable($product, $parentProduct);

                $attributes = $parentProduct
                    ->getTypeInstance(true)
                    ->getUsedProductAttributes($parentProduct);

                $freshProduct = null;
                foreach ($attributes as $attribute) {
                    if (!array_key_exists('configurable_attributes', $result)) {
                        $result['configurable_attributes'] = array();
                    }
                    $attr = array();
                    $attr['attribute_name'] = $attribute->getFrontend()->getLabel();
                    $attr['value'] = $product->getAttributeText($attribute->getAttributeCode());
                    if(empty($attr['value']) ) { // use the EAV tables only if the flat table doesn't work
                        if ($freshProduct == null)
                            $freshProduct = Mage::getModel('catalog/product')->load($product->getEntityId());
                        $attr['value'] = $attribute->getFrontend()->getValue($freshProduct);
                    }
                    $result['configurable_attributes'][] = $attr;
                }
            }
        }

        if (!isset($result['purchasable'])) {
            $result['purchasable'] = $this->_isPurchasable($product);
        }

        $images = $this->_getProductImages($product);
        if (isset($images['image']))
            $result['image'] = $images['image'];
        if (isset($images['small_image']))
            $result['small_image'] = $images['small_image'];
        if (isset($images['thumbnail']))
        $result['thumbnail'] = $images['thumbnail'];

        if ($includeExtras) {
            // Metas
            $metas = $this->_getMetas($storeId, $product, $parentProduct);
            if ($metas != null) {
                //if(isset($metas['meta1'])) $result['meta1'] = $metas['meta1'];
                //if(isset($metas['meta2'])) $result['meta2'] = $metas['meta2'];
                if(isset($metas['meta3'])) $result['meta3'] = $metas['meta3'];
                if(isset($metas['meta4'])) $result['meta4'] = $metas['meta4'];
                if(isset($metas['meta5'])) $result['meta5'] = $metas['meta5'];
            }

            // Brand and Category
            $brandAndCategoryProduct = (!$parentProduct || $product->isVisibleInSiteVisibility()) ? $product : $parentProduct;
            $setSettings = $this->_getProductAttributeSetSettings($brandAndCategoryProduct);

            if ($setSettings['brandAttribute'] != null) {
                $result['brand'] = $brandAndCategoryProduct->getAttributeText($setSettings['brandAttribute']);
            }

            if ($setSettings['catFromMagento']) {
                $cats = $this->_getCategoryInformation($storeId, $brandAndCategoryProduct);
                if (isset($cats['category'])) $result['category'] = $cats['category'];
                if (isset($cats['sub_category'])) $result['sub_category'] = $cats['sub_category'];
            } else {
                if ($setSettings['catFromAttributes']) {
                    if ($setSettings['categoryAttribute'] != null) {
                        $result['category'] = $brandAndCategoryProduct->getAttributeText($setSettings['categoryAttribute']);
                    }

                    if ($setSettings['subcategoryAttribute'] != null) {
                        $result['sub_category'] = $brandAndCategoryProduct->getAttributeText($setSettings['subcategoryAttribute']);
                    }
                }
            }

            // Inventory
            $result['in_stock'] = $product->isAvailable() ? "true" : "false";
            $stockItem = $product->getStockItem();
            if ($stockItem) {
                $result['qty_on_hand'] = $stockItem->getStockQty();
            }

            // Related Products
            $result['links'] = $this->_getProductLinks($product);
        }

        $result['type'] = $product->getTypeId();

        return $result;
    }

    public function getProductInformationFromQuoteItem(Mage_Sales_Model_Quote_Item $item, $additionalInformation = array()) {
        $children = $item->getChildren();
        return $this->_getProductInformationWork($item, $additionalInformation, count($children) > 0, $children);
    }

    public function getProductInformationFromOrderItem(Mage_Sales_Model_Order_Item $item, $additionalInformation = array()) {
        return $this->_getProductInformationWork($item, $additionalInformation, $item->getHasChildren(), $item->getChildrenItems());
    }

    public function getProductUrl(Mage_Catalog_Model_Product $product) {
        return substr(Mage::getSingleton('core/url')->parseUrl($product->getProductUrl())->getPath(), 1);
    }

    public function getProductImage(Mage_Catalog_Model_Product $product) {
        $images = $this->_getProductImages($product);
        if (isset($images['thumbnail']))
            return $images['thumbnail'];
        if (isset($images['small_image']))
            return $images['small_image'];
        if (isset($images['image']))
            return $images['image'];
        return null;
    }
    
    private function _getProductInformationWork($item, $getInfo, $hasChildren, $children) {
        $getProduct = in_array('product', $getInfo);
        $getImage = in_array('image_url', $getInfo);
        $getLink = in_array('product_url', $getInfo);

        $result = new Varien_Object();

        $result->setProductId((int)$item->getProductId());
        $result->setIsConfigurable(false);
        $result->setIsBundle(false);
        $result->setSku($item->getSku());

        if (Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE == $item->getProductType() && $hasChildren) {
            $result->setIsConfigurable(true);

            $result->setParentId($result->getProductId());
            $result->setProductId((int)$children[0]->getProductId());
        }

        if (Mage_Catalog_Model_Product_Type::TYPE_BUNDLE == $item->getProductType() && $hasChildren) {
            $result->setIsBundle(true);

            $product = Mage::getModel('catalog/product')->load($result->getProductId());
            $result->setSku($product->getSku());
            $result->setProduct($product);
        }
        else if ($getProduct || $getImage || ($getLink && !$result->getIsConfigurable())) {
            $result->setProduct(Mage::getModel('catalog/product')->load($result->getProductId()));
        }

        if ($getLink) {
            $result->setProductUrl($this->getProductUrl($result->getIsConfigurable()
                ? Mage::getModel('catalog/product')->load($result->getParentId())
                : $result->getProduct()));
        }

        if ($getImage) {
            $result->setImageUrl($this->getProductImage($result['product']));
        }

        return $result;
    }

    private function _getProductUrlWithCache(Mage_Catalog_Model_Product $product) {
        $productId = $product->getEntityId();

        if (!isset($this->_urlsById[$productId])) {
            $this->_urlsById[$productId] = $this->getProductUrl($product);
        }

        return $this->_urlsById[$productId];
    }

    private function _getProductImages(Mage_Catalog_Model_Product $product) {
        $parent = $this->_getParentProduct($product);
        $parentIsConfigurable = $parent && Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE == $parent->getTypeId();
        if ($this->_useConfigurableParentImages == null)
            $this->_useConfigurableParentImages = Mage::getStoreConfig(Mage_Checkout_Block_Cart_Item_Renderer_Configurable::CONFIGURABLE_PRODUCT_IMAGE)
                                                    == Mage_Checkout_Block_Cart_Item_Renderer_Configurable::USE_PARENT_IMAGE;

        $none = 'no_selection';

        $image = null;
        $smallImage = null;
        $thumbnail = null;

        if ($parent && $parentIsConfigurable && $this->_useConfigurableParentImages) {
            $image = $parent->getImage();
            $smallImage = $parent->getSmallImage();
            $thumbnail = $parent->getThumbnail();
        }
        else {
            $image = $product->getImage();
            if ($parent && (!$image || $image == $none)) $image = $parent->getImage();

            $smallImage = $product->getSmallImage();
            if ($parent && (!$smallImage || $smallImage == $none)) $smallImage = $parent->getSmallImage();

            $thumbnail = $product->getThumbnail();
            if ($parent && (!$thumbnail || $thumbnail == $none)) $thumbnail = $parent->getThumbnail();
        }

        $result = array();
        if ($image && $image != $none) $result['image'] = $image;
        if ($smallImage && $smallImage != $none) $result['small_image'] = $smallImage;
        if ($thumbnail && $thumbnail != $none) $result['thumbnail'] = $thumbnail;

        return $result;
    }

    private function _getParentProduct(Mage_Catalog_Model_Product $product) {
        $parentIds = Mage::getModel('catalog/product_type_configurable')
            ->getParentIdsByChild($product->getEntityId());

        if (is_array($parentIds) && count($parentIds) > 0) {
            $parentId = $parentIds[0];
            if ($parentId != null) {
                if (!array_key_exists($parentId, $this->_parentsById)) {
                    $this->_parentsById[$parentId] = Mage::getModel('catalog/product')
                        ->load($parentId);
                }
                return $this->_parentsById[$parentId];
            }
        }

        return null;
    }

    private function _isPurchasable(Mage_Catalog_Model_Product $product, Mage_Catalog_Model_Product $parent = null) {
        if ($product->getStatus() != Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            return "false";
            
        if ($parent == null) {
            return Mage::getSingleton('listrak/product_purchasable_visibility')
                        ->isProductPurchasableBySetting(Mage::getStoreConfig('remarketing/productcategories/purchasable_visibility'), $product)
                ? "true" : "false";
        }
        else {
            return $parent->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_ENABLED
                    && Mage::getSingleton('listrak/product_purchasable_visibility')
                            ->isProductPurchasableBySetting(Mage::getStoreConfig('remarketing/productcategories/purchasable_visibility'), $parent)
                ? "true" : "false";
        }
    }

    private function _getProductAttributeSetSettings(Mage_Catalog_Model_Product $product)
    {
        if ($this->_attributeSets == null) {
            $this->_attributeSets = array(0 => array(
                //default values
                'brandAttribute' => null,
                'catFromMagento' => true,
                'catFromAttributes' => false,
                'categoryAttribute' => null,
                'subcategoryAttribute' => null
            ));

            $attributeSetSettings = Mage::getModel('listrak/product_attribute_set_map')
                ->getCollection();
            foreach ($attributeSetSettings as $setSettings) {
                $this->_attributeSets[$setSettings->getAttributeSetId()] = array(
                    'brandAttribute' => $setSettings->getBrandAttributeCode(),
                    'catFromMagento' => $setSettings->finalCategoriesSource() == 'default',
                    'catFromAttributes' => $setSettings->finalCategoriesSource() == 'attributes',
                    'categoryAttribute' => $setSettings->getCategoryAttributeCode(),
                    'subcategoryAttribute' => $setSettings->getSubcategoryAttributeCode()
                );
            }
        }
        return array_key_exists($product->getAttributeSetId(), $this->_attributeSets)
            ? $this->_attributeSets[$product->getAttributeSetId()] : $this->_attributeSets[0];
    }

    private function _getCategoryInformation($storeId, Mage_Catalog_Model_Product $product) {
        $rootLevel = Mage::helper('remarketing')->getCategoryRootIdForStore($storeId);
        $rootPath = array(1);
        if ($rootLevel)
            $rootPath[] = $rootLevel;

        $categoryLevel = Mage::helper('remarketing')->getCategoryLevel();

        if ($this->_skipCategories == null) {
            $this->_skipCategories = array_unique(array_merge(
                Mage::helper('remarketing')->getInactiveCategories(),
                Mage::helper('remarketing')->getCategoriesToSkip()));
        }

        $categories = $product->getCategoryCollection();
        $path = $this->_getFirstPathByPosition($categories, $categoryLevel + 1, $rootPath);

        $result = array();
        if (isset($path[$categoryLevel - 1])) $result['category'] = $this->_getCategoryField($path[$categoryLevel - 1], 'name');
        if (isset($path[$categoryLevel])) $result['sub_category'] = $this->_getCategoryField($path[$categoryLevel], 'name');

        return $result;
    }

    private function _getFirstPathByPosition($categoryCollection, $maxLevel, $underPath)
    {
        if (sizeof($underPath) >= $maxLevel)
            return $underPath;

        $nextCategory = array();
        foreach($categoryCollection as $category) {
            $pathIds = $category->getPathIds();

            if (sizeof(array_intersect($pathIds, $this->_skipCategories)) > 0)
                // the category tree contains a category that we want skipped or is not active
                continue;

            if (sizeof($pathIds) > sizeof($underPath) && !in_array($pathIds[sizeof($underPath)], $nextCategory)) {
                $isUnderPath = true;
                for($i = 0; $i < sizeof($underPath); $i++)
                {
                    if ($pathIds[$i] != $underPath[$i])
                    {
                        $isUnderPath = false;
                        break;
                    }
                }

                if ($isUnderPath)
                    $nextCategory[] = $pathIds[sizeof($underPath)];
            }
        }

        if (sizeof($nextCategory) == 0)
            return $underPath;

        $winnerPath = array();
        $winnerPathPosition = 0;
        foreach($nextCategory as $category)
        {
            $testPath = $underPath;
            $testPath[] = $category;

            $testPathPosition = $this->_getCategoryField($category, 'position');

            if (sizeof($winnerPath) == 0 || $winnerPathPosition > $testPathPosition)
            {
                $winnerPath = $testPath;
                $winnerPathPosition = $testPathPosition;
            }
        }

        return $this->_getFirstPathByPosition($categoryCollection, $maxLevel, $winnerPath);
    }

    private function _getCategoryField($categoryId, $field)
    {
        $category = $this->_getCategory($categoryId);

        if ($category != null)
        {
            return $category->getData($field);
        }

        return null;
    }

    private function _getCategory($categoryId)
    {
        if (array_key_exists($categoryId, $this->_categories))
            return $this->_categories[$categoryId];
        else {
            $category = Mage::getModel('catalog/category')
                ->load($categoryId);

            if ($category != null)
            {
                $this->_categories[$categoryId] = $category;
                return $category;
            }
        }

        return null;
    }

    private function _getProductLinks($product) {
        if (!Mage::getStoreConfig('remarketing/productcategories/product_links'))
            return null;

        static $_catalog_product_table = null;
        if ($_catalog_product_table == null)
            // this is done because a query shows up in MySQL with 'SET GLOBAL SQL_MODE = ''; SET NAMES utf8;' that is very costly in a loop
            $_catalog_product_table = Mage::getModel('core/resource')->getTableName('catalog/product');

        static $_catalog_product_link_attribute_table = null;
        if ($_catalog_product_link_attribute_table == null)
            $_catalog_product_link_attribute_table = Mage::getModel('catalog/product_link')->getAttributeTypeTable('int');

        $linkTypes = $this->_getLinkTypes();

        $c = Mage::getModel('catalog/product_link')
            ->getCollection();
        $s = $c->getSelect();
        $a = $s->getAdapter();

        $s->where('main_table.product_id = ?', $product->getId())
            ->where('main_table.product_id <> main_table.linked_product_id')
            ->where('main_table.link_type_id IN (?)', array_keys($linkTypes));

        $s->join(array('product' => $_catalog_product_table),
            'main_table.linked_product_id = product.entity_id',
            'sku');

        $positionJoinOn = array();
        foreach($linkTypes as $linkTypeId => $linkType) {
            if ($linkType['positionAttributeId'] != null) {
                array_push($positionJoinOn,
                    $a->quoteInto('main_table.link_type_id  = ?', $linkTypeId) . ' AND ' .
                    $a->quoteInto('attributes.product_link_attribute_id = ?', $linkType['positionAttributeId']));
            }
        }
        $s->joinLeft(array('attributes' => $_catalog_product_link_attribute_table),
            'main_table.link_id = attributes.link_id AND ((' . implode(') OR (', $positionJoinOn) . '))',
            array('position' => 'value'));

        $links = array();
        foreach($c as $r) {
            array_push($links, array(
                'link_type' => $linkTypes[$r->getLinkTypeId()]['name'],
                'sku' => $r->getSku(),
                'position' => $r->getPosition()
            ));
        }

        return $links;
    }

    private function _getLinkTypes() {
        static $_types = null;

        if ($_types == null) {
            $allLinks = array(
                Mage_Catalog_Model_Product_Link::LINK_TYPE_UPSELL    => array('name' => 'up_sell', 'positionAttributeId' => null),
                Mage_Catalog_Model_Product_Link::LINK_TYPE_CROSSSELL => array('name' => 'cross_sell', 'positionAttributeId' => null),
                Mage_Catalog_Model_Product_Link::LINK_TYPE_RELATED   => array('name' => 'related', 'positionAttributeId' => null),
                Mage_Catalog_Model_Product_Link::LINK_TYPE_GROUPED   => array('name' => 'grouped', 'positionAttributeId' => null)
            );

            foreach($allLinks as $linkId => &$link) {
                $linkAttributes = Mage::getModel('catalog/product_link')
                    ->setLinkTypeId($linkId)
                    ->getAttributes();
                foreach($linkAttributes as $attribute) {
                    if ($attribute['code'] == 'position' && $attribute['type'] == 'int') {
                        $link['positionAttributeId'] = $attribute['id'];
                        break;
                    }
                }
            }

            $_types = $allLinks;
        }

        return $_types;
    }

    private function _getMetas($storeId, Mage_Catalog_Model_Product $product, Mage_Catalog_Model_Product $parentProduct = null) {
        try {
            $provider = Mage::helper('remarketing')->getMetaDataProvider();
            if ($provider)
                return $provider->product($storeId, $product, $parentProduct);
        }
        catch(Exception $e) {
            Mage::helper('remarketing')->generateAndLogException('Exception retrieving product meta data', $e);
        }

        return null;
    }
}
