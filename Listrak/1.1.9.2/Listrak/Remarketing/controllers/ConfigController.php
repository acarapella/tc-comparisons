<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.0.0
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2011 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_ConfigController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        if($this->getRequest()->has('version')) {
            echo Mage::getConfig()->getNode('modules')->Listrak_Remarketing->version;
        }
        else if ($this->getRequest()->has('enableOnescriptTracking')) {
            echo $this->_enableOnescriptTracking();
        }

        return $this;
    }

    public function registerAction()
    {
        $reg = Mage::getStoreConfig('remarketing/config/account_created');

        if (!$reg) {
            Mage::getConfig()->saveConfig('remarketing/config/account_created', '1');
            Mage::getConfig()->reinit();
        }
    }

    public function checkAction()
    {
        echo Mage::getStoreConfig('remarketing/config/account_created');
    }

    private function _enableOnescriptTracking() {
        if (!Mage::helper('remarketing')->onescriptEnabled())
            return "failure: Onescript is disabled";

        if (Mage::helper('remarketing')->onescriptReady())
            return "success: already enabled";

        if (!$this->getRequest()->has('skipValidation')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_ENCODING, "");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

            curl_setopt($ch, CURLOPT_URL, Mage::helper('remarketing')->onescriptSrc());
            $script = curl_exec($ch);
            $error = $script === false ? curl_error($ch) : '';

            // $ch shouldn't be used below this next line
            curl_close($ch);

            if ($script === false)
                return "failure: Onescript did not load: {$error}";

            if (strpos($script, "_ltk.SCA.Load(") === false)
                return "failure: Onescript does not load the SCA session ID";
        }

        Mage::getConfig()->saveConfig('remarketing/config/onescript_ready', '1');
        Mage::getConfig()->reinit();
        return "success";
    }
}