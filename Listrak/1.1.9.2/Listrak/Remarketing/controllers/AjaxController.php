<?php
/**
 * Listrak Remarketing Magento Extension Ver. 1.1.9
 *
 * PHP version 5
 *
 * @category  Listrak
 * @package   Listrak_Remarketing
 * @author    Listrak Magento Team <magento@listrak.com>
 * @copyright 2011 Listrak Inc
 * @license   http://s1.listrakbi.com/licenses/magento.txt License For Customer Use of Listrak Software
 * @link      http://www.listrak.com
 */

class Listrak_Remarketing_AjaxController extends Mage_Core_Controller_Front_Action
{
    public function cartAction() {
        $tracking_cart = $this->getLayout()->createBlock('remarketing/tracking_sca');

        $this->getResponse()->setHeader('Content-Type', 'application/javascript', true);
        $this->getResponse()->setBody($tracking_cart->getCartJavascript());
    }
    
    public function trackAction() {
        $tracking_cart = $this->getLayout()->createBlock('remarketing/tracking_sca');

        $this->getResponse()->setHeader('Content-Type', 'application/javascript', true);
        $this->getResponse()->setBody($tracking_cart->toHtml());
    }
}
